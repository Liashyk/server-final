package com.controller;

import com.model.Bot;
import com.service.interfaces.BotService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
public class BotController {

    private BotService botService;

    @Autowired(required = true)
    @Qualifier(value = "botService")
    public void setBotService(BotService botService) {
        this.botService = botService;
    }


    @RequestMapping(value = "bot", method = RequestMethod.GET)
    public String listCatalog(Model model) {
        model.addAttribute("allChat", botService.getChat());
        model.addAttribute("newChat", new Bot());

        return "bot";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addBook(@ModelAttribute("newChat") Bot bot){
        bot.setAnswer(botService.sayInReturn(bot.getQuestion()));
        botService.setToChat(bot);

        return "redirect:/bot";
    }
}
