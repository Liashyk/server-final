package com.service.interfaces;

import com.model.Bot;

import java.util.List;

public interface BotService {
    public String sayInReturn(String question);

    public List<Bot> getChat();

    public void setToChat(Bot bot);
}
